Projekt Komponenty Pejaś

Termin absolutny 30.05
Termin do oddania działajacych programów + dokumentacji (22.05 - nasz wewnętrzny termin, zeby zostalo na testowanie tego chwile i zeby nie bylo na ostatnia chwile)

Grupa I
- Michał Malinowski
- Grzesiek Bączek
- Paweł Adam Gerard Wojciechowski - dokumentacja
- Konrad Stojanowski
- Damian Stachelczyk

Zadania :

-3.1
- rejestracja,generacja,usuwanie klucza i podpisu
- testy i dokumentacja

Grupa II
- Sebastian Araszewski
- Tomasz Petrów
- Adam Jeziorski
- Bartosz Hliwa
- Błażej Sadek
- Marcin Grzelak - dokumentacja

Zadania :

- 3.2, 3.3
- program do transakcji
- dokumentacja + testy

Dodatkowo (dla wszystkich, badz chetnych/wybranych osób)

- założenie GIT'a (lub scalenie plików w katalog projektu)
- dokumentacja zbiorcza, wraz z celowością projektu (o czym projekt itp.)
- ....