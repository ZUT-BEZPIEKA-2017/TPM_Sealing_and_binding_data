#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libtransact.h>
#include <tss/platform.h>
#include <tss/tcpa_defines.h>
#include <tss/tcpa_typedef.h>
#include <tss/tcpa_struct.h>
#include <tss/tss_typedef.h>
#include <tss/tss_structs.h>
#include <tss/tspi.h>
#include <sys/stat.h>


#define ACCOUNT_FILE_NAME "transactions.log"
#define ACCOUNT_ENCDATA_SECRET_MODE TSS_SECRET_MODE_PLAIN
#define ACCOUNT_ENCDATA_SECRET "ENC PWD"
#define ACCOUNT_ENCDATA_SECRET_LEN strlen(ACCOUNT_ENCDATA_SECRET)


TSS_HCONTEXT	hContext;
TSS_HTPM	hTPM;
TSS_HKEY	hSRK;
TSS_HKEY	hKey;
TSS_UUID 	keyUUID = {1,2,3,4,5,{6,7,9,8,10,11}};
TSS_UUID SRK_UUID = TSS_UUID_SRK;
TSS_HPOLICY 	hEncUsagePolicy;
BYTE		*rgbDataToSeal; //should point to the data that should be sealed
BYTE		*prgbUnsealedData; //should point to the result of unsealing operation
TSS_HENCDATA	hEncData;
TSS_HPCRS	hPcrComposite;
UINT32		bloblength;
BYTE		*blob; //should point to the extracted encrypted data
UINT32		ulDataLength; //use it for sealing
UINT32		ulNewDataLength; //use it for unsealing
TSS_RESULT	result;
BYTE		*rgbPcrValue;
UINT32		ulPcrValueLength;



int exit_on_error_unsealing(FILE *f,int fd)
{
	close(fd);
	fclose(f);
	Tspi_Context_FreeMemory(hContext,NULL);
	Tspi_Context_Close(hContext);
	return 1;
}

int exit_on_error_sealing(FILE *f)
{
	fclose(f);
	Tspi_Context_FreeMemory(hContext,NULL);
	Tspi_Context_Close(hContext);
	return 1;
}

int exit_on_error_TSS()
{
	Tspi_Context_FreeMemory(hContext,NULL);
	Tspi_Context_Close(hContext);
	return 1;
}

/* Open account file (create, if it does not exist) */
FILE* openAccountFile(const char *fname)
{
	FILE* f = NULL;
	
	f = fopen(fname, "r+");   /* open for read/write 
				    (fails if file does not exist) */
	if (f == NULL)
		f = fopen(fname, "w+");  /* create file for read/write */
	return f;
}

UINT32 calculateResult()
{
	//Transform the 4 Byte value (pointed from prgbUnsealedData) into one UINT32 value
	UINT32 unsealedData[4];
	int i;

	for (i=0;i<4;i++){
		unsealedData[i]=(UINT32)*prgbUnsealedData;
		prgbUnsealedData++;
	}
	return (unsealedData[0]+(unsealedData[1]<<8)+(unsealedData[2]<<16)+(unsealedData[3]<<24));
}



/* Print contents of account file */
int printAccount(FILE *f)
{
	int c; //variable for a single char
	int fd; //file descriptor
	int fileSize=0;
	int i=0;
	int j=0;
	int blocklength=312;

	printf("Transactions logged for the account:\n");

	fseek(f, 0L, SEEK_SET);              /* seek to start of file */
	fd = fileno(f);
	struct stat s_buf;
	fstat(fd, &s_buf);
	fileSize = s_buf.st_size;

	if (fileSize!=0){
		
		BYTE fileContent[fileSize]; //Array that contains the sealed data
		BYTE dataToUnseal[blocklength]; //Array that contains one block sealed data
	
		printf("The size of the file is: %d Bytes\n",fileSize);	
		while((c=fgetc(f))!= EOF){
			//printf("%02x ",c);
			fileContent[j++]=c;
		}
		while (i<fileSize){
			for (j=0;j<blocklength;j++)
				dataToUnseal[j]=fileContent[i++];
			if (Tspi_SetAttribData(hEncData,TSS_TSPATTRIB_ENCDATA_BLOB,TSS_TSPATTRIB_ENCDATABLOB_BLOB,blocklength,(BYTE*)&dataToUnseal) != TSS_SUCCESS){
				printf("ERROR: Tspi_SetAttribData\n");	
				return exit_on_error_unsealing(f,fd);
			}
			//**** Here you should implement the Tspi-function for unsealing! ****
			//**** Further, print the result on stdout! Hint: Use the function calculateResult! ****


//JP
			if(Tspi_Data_Unseal(hEncData,hSRK,&ulNewDataLength, &prgbUnsealedData)){
				printf("ERROR: Tspi_Data_Unseal\n");	
				return exit_on_error_unsealing(f,fd);
			}
	int k =0;
	for(k=1;k<ulNewDataLength;k*=4)
		printf("%d",calculateResult());

//JP

		}
	}
	return 0;
}


/* Append amount to account file */
int addAmount(int amount, FILE *f)
{
	fseek(f, 0L, SEEK_END);                /* seek to end of file */
	                                      /* write amount */
	
	//**** Here you implement the Tspi-function for sealing and ****
	//**** the Tspi-function for extracting the encrypted data from hEncData ****


//JP
if(Tspi_GetPolicyObject(hEncData,TSS_POLICY_USAGE,&hEncUsagePolicy))
{
	printf("ERROR GetPolicyObject\n");
		return exit_on_error_sealing(f);
}


	ulDataLength= sizeof(amount);
	rgbDataToSeal= (BYTE*) &amount;
	if (Tspi_Data_Seal(hEncData, hSRK, ulDataLength, rgbDataToSeal, hPcrComposite)) {
		printf("ERROR Tspi_Data_Seal\n");
		return exit_on_error_sealing(f);
	}

	if (Tspi_GetAttribData(hEncData, TSS_TSPATTRIB_ENCDATA_BLOB, TSS_TSPATTRIB_ENCDATABLOB_BLOB, &bloblength, &blob)) {
		printf("ERROR Tspi_GetAttribData\n");
		return exit_on_error_sealing(f);
	}

//JP

	if (blob!=0){
		if (fwrite(blob,bloblength, 1, f) < 1) {
			printf("Error writing account file!\n");
			return exit_on_error_sealing(f);
		}
		printf("New amount (%u EUR) logged.\n", amount);
	}
	else printf("No account logged.\n");

	return 0;
}


int TSS_initialize()
{
	if (Tspi_Context_Create(&hContext) != TSS_SUCCESS){
		printf("ERROR Tspi_Context_Create\n");
		return 1;}
	if (Tspi_Context_Connect(hContext,NULL) != TSS_SUCCESS){
		printf("ERROR Tspi_Context_Connect\n");
		return exit_on_error_TSS();}
	if (Tspi_Context_GetTpmObject(hContext,&hTPM) != TSS_SUCCESS){
		printf("ERROR Tspi_Context_GetTpmObject\n");
		return exit_on_error_TSS();}

	//**** Here you should implement the Setup functions necessary for sealing and unsealing ****

//Utwórz zaszyfrowany obiekt danych
	if (Tspi_Context_CreateObject(hContext, TSS_OBJECT_TYPE_ENCDATA, TSS_ENCDATA_SEAL, &hEncData)) {
		printf("ERROR Tspi_Context_CreateObject(hEncData)\n");
		return exit_on_error_TSS();
	}

//Utwórz politykę użytkowania obiektu, która będą potrzebne do ustawienia danych uwierzytelniających dostęp do obiektu danych zaszyfrowanych.




//Dla zaszyfrowanego obiektu danych należy ustawić sekret związany z nowo utworzoną polityką. 
//Ustaw sekret i przypisz politykę do zaszyfrowanego obiektu danych. Podpowiedź: sekret i tryb sekretu są zdefiniowane na początku kodu źródłowego.




//Ponieważ należy pieczętować dane z 13 rejestrem PCR, to należy utworzyć złożony obiekt PCR.




//Odczytaj wartość 13 rejestru PCR ...




// ... i ustaw wartość w złożonym obiekcie PCR.




//W końcu, w oparciu o UUID załaduj klucz magazynu utworzony w rozdz. 3.1






	return 0;
}


/* Main program: 
 * 	open account file, 
 * 	print contents, 
 * 	execute transaction (by calling shared library),
 * 	append new amount to account file
 * 	close account file
 */
int main(int argc, char** argv)
{
        int amount = 0;
	FILE* file = NULL;
	
	printf("\n");
	
	if (TSS_initialize()!=0){
		printf("Error: TSS_initialize\n");
		return 1;
	}

	printf("\n");
	file = openAccountFile(ACCOUNT_FILE_NAME);
	
	if (file == NULL) {
		printf("Error: Could not open account file.\n");
		return 1;
	}

	if (printAccount(file)!=0){
		printf("Error: Unsealing\n");
		return 1;
	}

	printf("\nAccounting: starting new transaction...\n");  
	amount = executeTransaction();   /* call function from shared library */
	printf("Result of transaction: %u\n", amount);

	if (addAmount(amount, file)!=0){
		printf("Error: Sealing\n");
		return 1;
	}

	printf("\n");
	fclose(file);
	Tspi_Context_FreeMemory(hContext,NULL);
	Tspi_Context_Close(hContext);
	return 0;
}
