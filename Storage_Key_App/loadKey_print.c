#include <stdio.h>
#include <string.h>
#include <tss/platform.h>
#include <tss/tss_typedef.h>
#include <tss/tss_structs.h>
#include <tss/tspi.h>
#include <trousers/trousers.h>
#include <errno.h>

TSS_HCONTEXT hContext;

int freeContext() {
	Tspi_Context_FreeMemory(hContext,NULL);
	Tspi_Context_Close(hContext);
}

print_KM_KEYINFO(TSS_KM_KEYINFO *k)
{
	printf("Version     : %hhu.%hhu.%hhu.%hhu\n", k->versionInfo.bMajor, k->versionInfo.bMinor,
	       k->versionInfo.bRevMajor, k->versionInfo.bRevMinor);
	printf("UUID        : %08x %04hx %04hx %02hhx %02hhx %02hhx%02hhx%02hhx%02hhx%02hhx%02hhx\n",
	       k->keyUUID.ulTimeLow, k->keyUUID.usTimeMid, k->keyUUID.usTimeHigh,
	       k->keyUUID.bClockSeqHigh, k->keyUUID.bClockSeqLow,
	       k->keyUUID.rgbNode[0] & 0xff, k->keyUUID.rgbNode[1] & 0xff,
	       k->keyUUID.rgbNode[2] & 0xff, k->keyUUID.rgbNode[3] & 0xff,
	       k->keyUUID.rgbNode[4] & 0xff, k->keyUUID.rgbNode[5] & 0xff);
	printf("parent UUID : %08x %04hx %04hx %02hhx %02hhx %02hhx%02hhx%02hhx%02hhx%02hhx%02hhx\n",
	       k->parentKeyUUID.ulTimeLow, k->parentKeyUUID.usTimeMid, k->parentKeyUUID.usTimeHigh,
	       k->parentKeyUUID.bClockSeqHigh, k->parentKeyUUID.bClockSeqLow,
	       k->parentKeyUUID.rgbNode[0] & 0xff, k->parentKeyUUID.rgbNode[1] & 0xff,
	       k->parentKeyUUID.rgbNode[2] & 0xff, k->parentKeyUUID.rgbNode[3] & 0xff,
	       k->parentKeyUUID.rgbNode[4] & 0xff, k->parentKeyUUID.rgbNode[5] & 0xff);
	printf("auth        : %s\n", k->bAuthDataUsage ? "YES" : "NO");
	if (k->ulVendorDataLength)
		printf("vendor data : \"%s\" (%u bytes)\n", k->rgbVendorData,
		       k->ulVendorDataLength);
	else
		printf("vendor data : (0 bytes)\n");

	printf("\n");
}


int main(int argc, char** argv)
{
	TSS_HKEY hSRK, hKey;
	TSS_FLAG initFlags;
	TSS_UUID SRK_UUID = TSS_UUID_SRK;
	TSS_UUID keyUUID = {1,2,3,4,5,{6,7,9,8,10,11}};
	TSS_HPOLICY hPolicy;
	BYTE secret[] = TSS_WELL_KNOWN_SECRET;
	TSS_RESULT result;


	UINT32			pulKeyHierarchySize, i;
	TSS_KM_KEYINFO*		ppKeyHierarchy;
        
	initFlags = 0;
	
	result = Tspi_Context_Create(&hContext);
	printf("Tspi_Context_Create: %s\n", Trspi_Error_String(result));
	if (result!=TSS_SUCCESS)
		return 1;
	
	atexit( freeContext);
	
	result = Tspi_Context_Connect(hContext, NULL);
	printf("Tspi_Context_Connect: %s\n", Trspi_Error_String(result));
	if (result!=TSS_SUCCESS)
        return 2;
	
	result = Tspi_Context_CreateObject(hContext, TSS_OBJECT_TYPE_POLICY, TSS_POLICY_USAGE, &hPolicy);
	printf("Tspi_Context_CreateObject: %s\n", Trspi_Error_String(result));
	result = Tspi_Policy_SetSecret(hPolicy, TSS_SECRET_MODE_SHA1, 20, secret);
	printf("Tspi_Policy_SetSecret: %s\n", Trspi_Error_String(result));


	/* Load the new key’s parent key, the Storage Root Key */
 	result = Tspi_Context_LoadKeyByUUID (hContext, TSS_PS_TYPE_SYSTEM, SRK_UUID, &hSRK );
	printf("Tspi_Context_LoadKeyByUUID: %s\n", Trspi_Error_String(result));
	if (result!=TSS_SUCCESS)
        return 3;

	Tspi_Policy_AssignToObject(hPolicy, hSRK);
	printf("AssignToObject: %s\n", Trspi_Error_String(result));
	if (result!=TSS_SUCCESS)
        return 4;

 	//result = Tspi_Context_LoadKeyByUUID (hContext, TSS_PS_TYPE_SYSTEM, keyUUID, &hKey);
	result = Tspi_Context_GetRegisteredKeysByUUID(hContext, TSS_PS_TYPE_SYSTEM, NULL,
						      &pulKeyHierarchySize, &ppKeyHierarchy);
	if (result != TSS_SUCCESS) {
		printf("Tspi_Context_RegisterKey: %s\n", Trspi_Error_String(result));
		Tspi_Context_CloseObject(hContext, hKey);
		Tspi_Context_Close(hContext);
		exit(result);
	}

	for (i = 0; i < pulKeyHierarchySize; i++) {
		printf("Registered key %u:\n", i);
		print_KM_KEYINFO(&ppKeyHierarchy[i]);
	}        


        result = Tspi_Context_GetKeyByUUID( hContext, TSS_PS_TYPE_SYSTEM,
						keyUUID,
						&hKey );
	printf("Tspi_Context_GetKeyByUUID: %s\n", Trspi_Error_String(result));
	if (result!=TSS_SUCCESS)
        return 5;

	result=Tspi_Key_LoadKey(hKey, hSRK);
	printf("Tspi_Key_LoadKey: %s\n", Trspi_Error_String(result));
	if (result!=TSS_SUCCESS)
        return 6;
	
    return 0;
}

// 294 
