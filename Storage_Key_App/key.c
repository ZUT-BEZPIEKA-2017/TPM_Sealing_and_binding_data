#include <stdio.h>
#include <string.h>
#include <tss/platform.h>
#include <tss/tcpa_defines.h>
#include <tss/tcpa_typedef.h>
#include <tss/tcpa_struct.h>
#include <tss/tss_typedef.h>
#include <tss/tss_structs.h>
#include <tss/tspi.h>



int main(int argc, char** argv)
{
        TSS_HCONTEXT hContext;
        TSS_HKEY hSRK, hKey;
        TSS_FLAG initFlags;
        TSS_UUID SRK_UUID = TSS_UUID_SRK;
        TSS_UUID keyUUID = {1,2,3,4,5,{6,7,9,8,10,11}};
        TSS_RESULT result;
        TSS_HPOLICY hPolicySRK, hPolicyKey;
		BYTE secret[] = TSS_WELL_KNOWN_SECRET;
		
		initFlags = 0;
        
		//Tworzene obiektu kontekstowego
        result = Tspi_Context_Create(&hContext);
        if (result!=TSS_SUCCESS){
            printf("\nError: Tspi_Context_Create\n");
            return 1;
        }
        
		//Łączenie z obiektem kontekstowym
        result = Tspi_Context_Connect(hContext, NULL);
        if (result!=TSS_SUCCESS){
            printf("\nError: Tspi_Context_Connect\n");
            return 1;
        }

		//Tworzenie obiektu polityki, który zostanie połączony z kluczem SRK
		result = Tspi_Context_CreateObject(hContext, TSS_OBJECT_TYPE_POLICY, TSS_POLICY_USAGE, &hPolicySRK);
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Context_CreateObject - hPolicySRK\n");
            return 1;
		}
		
		//Ustawianie sekretu dla polityki
		result = Tspi_Policy_SetSecret(hPolicySRK, TSS_SECRET_MODE_SHA1, 20, secret);
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Policy_SetSecret - hPolicySRK\n");
            return 1;
		}
		
		//Ładowanie klucza SRK
		result = Tspi_Context_LoadKeyByUUID(hContext, TSS_PS_TYPE_SYSTEM, SRK_UUID, &hSRK );
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Context_LoadKeyByUUID - SRK\n");
            return 1;
		}
		
		//Łączenie klucza SRK z polityką
		result = Tspi_Policy_AssignToObject(hPolicySRK, hSRK);
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Policy_AssignToObject - SRK\n");
            return 1;
		}
			

        //Tworzenie obiektu dla klucza
        result = Tspi_Context_CreateObject(hContext, TSS_OBJECT_TYPE_RSAKEY,  TSS_KEY_SIZE_2048 | TSS_KEY_TYPE_SIGNING | TSS_KEY_NOT_MIGRATABLE | TSS_KEY_NO_AUTHORIZATION, &hKey);
        //result = Tspi_Context_CreateObject(hContext, TSS_OBJECT_TYPE_RSAKEY, TSS_KEY_SIZE_2048 | TSS_KEY_TYPE_STORAGE | TSS_KEY_NOT_MIGRATABLE | TSS_KEY_NO_AUTHORIZATION, &hKey);
        if (result != TSS_SUCCESS){
			printf("\nError: Tspi_Context_CreateObject - key object\n");
            return 1;
        }        

		//Tworzenie obiektu polityki dla wygenerowanego klucza
		result = Tspi_Context_CreateObject(hContext, TSS_OBJECT_TYPE_POLICY, TSS_POLICY_MIGRATION, &hPolicyKey);
		if (result != TSS_SUCCESS) {
			printf("Error: Tspi_Context_CreateObject\n");
			return 1;
		}
		
		//Ustawianie sekretu dla polityki
		result = Tspi_Policy_SetSecret(hPolicyKey, TSS_SECRET_MODE_SHA1, 20, secret);
		if (result != TSS_SUCCESS) {
			printf("Error: Tspi_Policy_SetSecret - hPolicyKey\n");
			return 1;
		}
		
		//Łączenie wygenerowanego klucza z polityką
		result = Tspi_Policy_AssignToObject(hPolicyKey, hKey);
		if (result != TSS_SUCCESS) {
			printf("Error: Tspi_Policy_AssignToObject - hKey\n");
			return 1;
		}
		
        //Generowanie nowego klucza
        printf("\nCreate a new key pair. Please wait!...\n");
        result = Tspi_Key_CreateKey(hKey, hSRK, 0); 
        if (result != TSS_SUCCESS){
			printf("\nError: Tspi_Key_CreateKey\n");
            return 1;
        }        
		
		//Rejestracja klucza
        printf("\nRegister the key in persistent storage...\n");
		result = Tspi_Context_RegisterKey(hContext, hKey, TSS_PS_TYPE_SYSTEM, keyUUID, TSS_PS_TYPE_SYSTEM, SRK_UUID);
        if (result!=TSS_SUCCESS) {
            printf("\nError: Tspi_Context_RegisterKey\n");
            if (result == TCS_ERROR(TSS_E_KEY_ALREADY_REGISTERED))
                printf("Key already registered!\n");
            return 1;
        }
        
		//Zwalnianie pliku kontekstowego
		Tspi_Context_Close(hContext);
		
        return 0;
}
