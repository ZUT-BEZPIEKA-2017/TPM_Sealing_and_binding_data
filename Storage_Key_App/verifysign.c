#include <stdio.h>
#include <string.h>
#include <tss/platform.h>
#include <tss/tcpa_defines.h>
#include <tss/tcpa_typedef.h>
#include <tss/tcpa_struct.h>
#include <tss/tss_typedef.h>
#include <tss/tss_structs.h>
#include <tss/tspi.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <limits.h> 

/*
przyjmuje dwa parametry:
pierwszy - ścieżka do pliku, którego podpis będzie weryfikowany
drugi - ścieżka do pliku, zawierający podpis do weryfikacji
*/
int main(int argc, char** argv)
{
        TSS_HCONTEXT hContext;
        TSS_HKEY hSRK, hKey;
        TSS_FLAG initFlags;
        TSS_UUID SRK_UUID = TSS_UUID_SRK;
        TSS_UUID keyUUID = {1,2,3,4,5,{6,7,9,8,10,11}};
        TSS_RESULT result;
        TSS_HHASH hHash;
		BYTE secret[] = TSS_WELL_KNOWN_SECRET;
		TSS_HPOLICY hPolicy;
		initFlags = 0;
        
		//--------------------
		BYTE *pubKey;
		UINT32 pubKeySize;
		//--------------------
        
		
		//Wczytanie pliku do weryfikacji----------------------------
		struct stat buf;
		int fd = open(argv[1], O_RDONLY);
		if (fd == -1)
		{
			printf("\nBlad funkcji open()\n");
			return 1;
		}
		int fs = fstat(fd, &buf);
		if (fs == -1)
		{
			printf("\nBlad funkcji fstat()\n");
			return 1;
		}
		int fileLength = (int)buf.st_size - 1;
		BYTE *buff = malloc(fileLength * sizeof(BYTE));
		read(fd, buff, fileLength);
		close(fd);		
		//-------------------------------------------------
		
		//Wczytnie podpisu
		fd = open(argv[2], O_RDONLY);
		if (fd == -1)
		{
			printf("\nBlad funkcji open()\n");
			return 1;
		}
		fs = fstat(fd, &buf);
		if (fs == -1)
		{
			printf("\nBlad funkcji fstat()\n");
			return 1;
		}
		int sigLen = (int)buf.st_size;
		BYTE *sig = malloc(sigLen * sizeof(BYTE));	
		read(fd, sig, sigLen);
		close(fd);
		//----------------------------------------------
		
		result = Tspi_Context_Create(&hContext);
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Context_Create\n");
            return 1;
		}
			
		result = Tspi_Context_Connect(hContext, NULL);
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Context_Connect\n");
            return 1;
		}
		
		//Tworzenie obiektu dla polityki
		result = Tspi_Context_CreateObject(hContext, TSS_OBJECT_TYPE_POLICY, TSS_POLICY_USAGE, &hPolicy);
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Context_CreateObject\n");
            return 1;
		}
		
		//UStawianie sekretu
		result = Tspi_Policy_SetSecret(hPolicy, TSS_SECRET_MODE_SHA1, 20, secret);
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Policy_SetSecret\n");
            return 1;
		}
		
		//Ładowanie klucza SRK
		result = Tspi_Context_LoadKeyByUUID(hContext, TSS_PS_TYPE_SYSTEM, SRK_UUID, &hSRK );
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Context_LoadKeyByUUID - SRK\n");
            return 1;
		}
		
		//Wiązanie polityki z kluczem SRK
		result = Tspi_Policy_AssignToObject(hPolicy, hSRK);
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Policy_AssignToObject\n");
            return 1;
		}
	
		//Ładowanie wygenerowanego klucza
		result = Tspi_Context_GetKeyByUUID( hContext, TSS_PS_TYPE_SYSTEM, keyUUID, &hKey );
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Context_GetKeyByUUID\n");
            return 1;
		}
	
		//Ładowanie wygenerowanego klucza
		result=Tspi_Key_LoadKey(hKey, hSRK);
		if (result != TSS_SUCCESS) {
			printf("\nError: Tspi_Context_GetKeyByUUID\n");
            return 1;
		}
	
		//Tworzenie obiektu dla skrótu
		result = Tspi_Context_CreateObject(hContext, TSS_OBJECT_TYPE_HASH, TSS_HASH_SHA1, &hHash);
		if (result != TSS_SUCCESS){
            printf("\nError: Tspi_Context_CreateObject - hHash\n");
            return 1;
        }

		//Załadowanie wartości pliku do obiektu skrótu
		result = Tspi_Hash_UpdateHashValue(hHash, fileLength, buff);
		if (result != TSS_SUCCESS){
            printf("\nError: Tspi_Hash_UpdateHashValue\n");
            return 1;
        }
		
		int j = 0;
		//Wyświetlenie wczytanego podpisu
		for (j = 0; j < sigLen; j++)
			printf("%x ", sig[j]);
		printf("\n");
		
		//Weryfikacja podpisu
		result = Tspi_Hash_VerifySignature(hHash, hKey, (UINT32)sigLen, sig);
		if (result != TSS_SUCCESS) {
			printf("Weryfikacja podpisu: niepowodzenie");
		}
		else{
			printf("Weryfikacja podpisu: powodenie");
		}
		
		
		printf("\nClosing Context...\n");
		Tspi_Context_Close(hContext);
		free(buff);
		
        return 0;
}
