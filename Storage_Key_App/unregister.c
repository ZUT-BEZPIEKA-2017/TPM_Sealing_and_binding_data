#include <stdio.h>
#include <string.h>
#include <tss/platform.h>
#include <tss/tcpa_defines.h>
#include <tss/tcpa_typedef.h>
#include <tss/tcpa_struct.h>
#include <tss/tss_typedef.h>
#include <tss/tss_structs.h>
#include <tss/tspi.h>



int main(int argc, char** argv)
{
        TSS_HCONTEXT hContext;
        TSS_HKEY hSRK, hKey;
        TSS_FLAG initFlags;
        TSS_UUID SRK_UUID = TSS_UUID_SRK;
        TSS_UUID keyUUID = {1,2,3,4,5,{6,7,9,8,10,11}};
        TSS_RESULT result;
        
		initFlags = 0;
        
        result = Tspi_Context_Create(&hContext);
        if (result!=TSS_SUCCESS){
                printf("\nError: Tspi_Context_Create\n");
                return 1;
        }
        
        result = Tspi_Context_Connect(hContext, NULL);
        if (result!=TSS_SUCCESS){
                printf("\nError: Tspi_Context_Connect\n");
                return 1;
        }
        
        /* Load the new keyâ€™s parent key, the Storage Root Key */
        result = Tspi_Context_LoadKeyByUUID(hContext, TSS_PS_TYPE_SYSTEM, SRK_UUID, &hSRK);
        if (result != TSS_SUCCESS){
                printf("\nError: Tspi_Context_LoadKEyByUUID - SRK \n");
                return 1;
        }        

		/* Load generated key */
        result = Tspi_Context_LoadKeyByUUID(hContext, TSS_PS_TYPE_SYSTEM, SRK_UUID, &hKey);
		if (result != TSS_SUCCESS){
                printf("\nError: Tspi_Context_LoadKEyByUUID - Generated Key\n");
                return 1;
        }   
		
        /* Unegister key in system persistent storage */
        printf("\nUnregister the key in persistent storage...\n");
        result = Tspi_Context_UnregisterKey(hContext, TSS_PS_TYPE_SYSTEM, keyUUID, &hKey);
        if (result!=TSS_SUCCESS){
                printf("\nError: Tspi_Context_UnregisterKey\n");
                return 1;
        }
 
		printf("\nClosing Context...\n");
		Tspi_Context_Close(hContext);
		
        return 0;
}
